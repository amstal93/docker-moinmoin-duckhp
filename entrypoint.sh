#! /bin/sh

if [ ! -d "/moin/config/" ] && [ ! -d "/moin/config/wikiconfig.py" ]
then

  echo -e "\e[94;1m# This is fresh moin docker! Need to extract some stuff...\e[0m"
  cd /moin/

  if [ -z "$(ls -A .)" ]
  then
    cd /moin/
    _bkupfn="backup-$(date|md5sum|cut -b5-10|tr -d '\n')-$(date -r /entrypoint.sh +%Y%m%d%H%M%S).tar.gz"
    GZIP=-9 tar -zcvf "$_bkupfn" --exclude="backup-*" .
    unset GZIP
  fi

#############################
  echo -e "\e[94;1m# Extracting base64 encoded data to the '/moin' folder. Please wait...\e[0m"

#  v-- !!WARNING DONT EVER CHANGE THESE TWO LINES!!
##PACKEDDATABEG##
##ENDPACKEDDATA##
#  ^-- !!WARNING DONT EVER CHANGE THESE TWO LINES!!

  echo $BASE64TARGZ \
  | base64 -d - \
  | tar zxvf -
  echo -e "\e[92;1m Done extracting !!!\e[0m"
  unset BASE64TARGZ
#############################

  ### patching some stuff
  echo -e "\e[94;1m# Making some corrections to some of the files etc. ..\e[0m"
  sed -i -e 's/logfile=\/tmp\/moin.log/logfile=\/moin\/moin.log/g' /moin/config/logging/logfile

  sed -i -e 's/# -\*- coding: iso-8859-1 -\*-/# -\*- coding: utf-8 -\*-/g' /moin/config/wikiconfig.py
  sed -i -e 's/wikiconfig_dir = os\.path\.abspath(os\.path\.dirname(__file__))/wikiconfig_dir="/moin/config/"/g' /moin/config/wikiconfig.py
  sed -i -e 's/instance_dir = wikiconfig_dir/instance_dir="\/moin\/"/g' /moin/config/wikiconfig.py
  sed -i -e 's/data_dir = os\.path\.join(instance_dir, \x27data\x27, \x27\x27) # path with trailing \//data_dir="\/moin\/data\/"/g' /moin/config/wikiconfig.py
  sed -i -e 's/data_underlay_dir = os\.path\.join(instance_dir, \x27underlay\x27, \x27\x27) # path with trailing \//data_underlay_dir="\/moin\/underlay\/"/g' /moin/config/wikiconfig.py
  sed -i -e 's/#superuser = \[u"YourName", \]/#superuser=\[u"YourLoginName", \]/g' /moin/config/wikiconfig.py
  ###

  echo -e "\e[94;1m# Making sure py file are utf8 ...\e[0m"

  find /moin -type f -iname '*.py' -exec sh -c "echo {} >> /tmp/pyfiles.tmp" \;

  while read _pyfile
  do
    echo -e "Doing file: $_pyfile"
    sed -i -e '/^# \-\*\- coding: .*/d' $_pyfile
    sed -i '1s;^;# -*- coding: utf-8 -*-\n;' $_pyfile ;
  done </tmp/pyfiles.tmp

  unset _pyfile

  ###

  echo -e "\e[92;1m# Done extracting and patching the /moin content\e[0m"
  echo -e "\e[97;1m# Backup of any previous files here: /moin/$_bkupfn \e[0m"
  unset _bkupfn

fi
#############################

#export MOINLOGGINGCONF=/moin/config/logging/stderr

echo -e "\e[96;1m# Starting shit up!..\e[0m"

export PYTHONPATH=/usr/local/lib/python2.7/site-packages:/moin/guni-wsgi:/moin/config
export GATEWAY=$(ip route list | awk '/default/ { print $3 }')

if [ -z "$*" ]; then
    exec gunicorn -b :8080 --forwarded-allow-ips="*" moin_wsgi
else
    exec "$@"
fi
