###########################################################
#
# dockerized moin wiki.
#
###########################################################

ARG baseimage=python:2.7.18-alpine

ARG moinversion=1.9.11

###########################################################
# Get xapian-bindings-python package from older Alpine
#FROM alpine:3.10 as xapianbindingsrc
#RUN mkdir /dist \
#    && apk update --no-cache --progress \
#    && apk upgrade --no-cache --progress \
#    && echo "Getting from Alpine 3.10: xapian-bindings-python, libxapian .." \
#    && apk add xapian-bindings-python libxapian \
#    && apk fetch -R -o '/dist/' xapian-bindings libxapian

###########################################################

FROM $baseimage AS base
RUN mkdir -p /dist/

###########################################################
FROM base AS build_stage1

COPY ./xapian-core-1.4.19.tar.xz /dist/xapian/
COPY ./xapian-bindings-1.4.19.tar.xz /dist/xapian/
COPY ./moin-1.9.11.tar.gz /dist/moin/

RUN apk update --no-cache --progress \
    && apk upgrade --no-cache --progress \
    && apk add --no-cache --progress tzdata git make build-base automake autoconf zlib-dev swig libffi-dev zip \
    && pip install pygments gunicorn sphinx \
    && pip list --outdated --format=freeze \
    | grep -v '^\-e' | cut -d = -f 1 \
    | xargs -n1 pip install  -U

RUN mkdir -p /dist/xapian \
    && cd /dist/xapian/ \
    && find . ! -name . -prune -type d -exec echo rm -R {} + \
    && echo -e "\e[94;1m# Building xapian core library...\e[0m" \
    && if [ ! -f /dist/xapian/xapian-core-1.4.19.tar.xz ];then wget https://oligarchy.co.uk/xapian/1.4.19/xapian-core-1.4.19.tar.xz;fi \
    && tar xvf xapian-core-1.4.19.tar.xz \
    && cd /dist/xapian/xapian-core-1.4.19 \
    && aclocal \
    && automake \
    && ./configure --disable-documentation \
    && make clean \
    && make \
    && make install \
    && cd /dist/xapian \
    && echo -e "\e[94;1m# Building python2 xapian bindings...\e[0m" \
    && if [ ! -f /dist/xapian/xapian-bindings-1.4.19.tar.xz ];then wget https://oligarchy.co.uk/xapian/1.4.19/xapian-bindings-1.4.19.tar.xz;fi \
    && tar xvf xapian-bindings-1.4.19.tar.xz \
    && cd xapian-bindings-1.4.19 \
    && export XAPIAN_CONFIG=/dist/xapian/xapian-core-1.4.19/xapian-config \
    && echo "mkdir build && cd build" \
    && ./configure --with-python \
    --without-python3 --without-php --without-php7 --without-ruby \
    --without-tcl --without-csharp --without-java --without-perl \
    --without-lua --disable-documentation \
    && make clean \
    && make check \
    && make \
    && make install

## the gunicorn wsgi file to get included in the wiki-files tar
COPY ./guni-wsgi /dist/wiki-files/guni-wsgi

RUN cd /dist/moin \
    && echo "Building MoinMoin v$moinversion .." \
    && if [ ! -f /dist/moin/moin-1.9.11.tar.gz ];then wget https://github.com/moinwiki/moin-1.9/releases/download/1.9.11/moin-1.9.11.tar.gz;fi \
    && tar xvf moin-1.9.11.tar.gz \
    && echo "DISABLED(using copy or wget): git clone --progress --depth 1 --branch $moinversion https://github.com/moinwiki/moin-1.9" \
    && python2 -m pip --no-python-version-warning --disable-pip-version-check install passlib[bcrypt] \
    && cd moin-1.9.11 \
    && python setup.py clean \
    && python setup.py check \
    && if [[ $? -eq 0 ]];then echo -e '\e[92;1m# Checks seem OK\e[0m';else echo -e '\e[91;1m# SOMETHING FUCKY GOING ON!! Aborting..\e[0m' && exit 1;fi \
    && python setup.py build \
    && echo "DISABLED(dont work):python setup.py bdist_wheel -w /dist/wheels --no-clean" \
    && python setup.py install \
    && echo "DISABLED(dont use):pip wheel -r /dist/wheels/requirements.txt -w /dist/wheels" \
    && cd / \
    && find . -name '*.pyc' -delete \
    && find /dist/moin/moin-1.9.11/wiki/ -type f -name '*.zip' -exec sh -c "_dirname=\$(dirname '{}') && \
                                                                          _zipname=\$(basename '{}') && \
                                                                          _realname=\$(basename '{}' .zip) && \
                                                                          cd \$_dirname && \
                                                                          _oldsize=\$(du \$_zipname | cut -f1) && \
                                                                          echo -e \"\e[94;1m# Rezipping (just in case) with '-9' setting: \$_zipname Orig. Size: \$_oldsize \e[0m\" && \
                                                                          mkdir \$_realname && \
                                                                          UNZIP=-q unzip \$_zipname -d \$_realname && \
                                                                          rm \$_zipname && \
                                                                          zip -v -9 -r -dd -dg -ds 32k \$_zipname \$_realname && \
                                                                          rm -rf \$_realname && \
                                                                          _newsize=\$(du \$_zipname | cut -f1) && \
                                                                          echo -e \"\e[94;1m# Rezipped \$_zipname New Size: \$_newsize \e[0m\"" \; \
    && GZIP=-9 tar zcvf /dist/xapian-py-packs.tar.gz /usr/local/lib/libxapia* \
            /usr/local/lib/python2.7/site-packages/xapi* \
            /usr/local/share/xapian-core \
            /usr/local/bin/xapian-delve \
            /usr/local/bin/xapian-check \
            /usr/local/bin/xapian-compact \
            /usr/local/bin/xapian-progsrv \
            /usr/local/bin/xapian-replicate \
            /usr/local/bin/xapian-replicate-server \
            /usr/local/bin/xapian-tcpsrv \
    && GZIP=-9 tar zcvf /dist/moin-py-packs.tar.gz /usr/local/lib/python2.7/site-packages/moin* \
            /usr/local/lib/python2.7/site-packages/Moin* \
            /usr/local/lib/python2.7/site-packages/passli* \
            /usr/local/lib/python2.7/site-packages/bcryp* \
            /usr/local/lib/python2.7/site-packages/six* \
            /usr/local/lib/python2.7/site-packages/cff* \
            /usr/local/lib/python2.7/site-packages/pycpars* \
            /usr/local/lib/python2.7/site-packages/gunico* \
            /usr/local/bin/moin \
            /usr/local/share/moin/server/moin \
    && cp -r /dist/moin/moin-1.9.11/wiki/* /dist/wiki-files/ \
    && cd /dist/wiki-files/ \
    &&  find . -name '*.pyc' -delete \
    && GZIP=-9 tar zcvf /dist/moin-wiki-files.tar.gz *


###########################################################
FROM base AS build_stage2

LABEL maintainers="Duck Hunt-Pr0" \
      name="MoinMoin wiki v$moinversion standalone on Alpine"

COPY --from=build_stage1 /dist/xapian-py-packs.tar.gz /dist/
COPY --from=build_stage1 /dist/moin-py-packs.tar.gz /dist/
COPY --from=build_stage1 /dist/moin-wiki-files.tar.gz /dist/
COPY ./entrypoint.sh /entrypoint.sh

RUN apk update --progress --no-cache \
    && apk upgrade --progress --no-cache \
    && apk add --progress --no-cache tzdata build-base libffi-dev \
    && pip2 --no-python-version-warning install --upgrade --force-reinstall pip \
    && cd / && tar zxvf /dist/xapian-py-packs.tar.gz \
    && cd / && tar zxvf /dist/moin-py-packs.tar.gz \
    && rm /dist/*py-packs.* \
    && pip2 --no-python-version-warning --disable-pip-version-check freeze \
    | grep '==' \
    | cut -d'=' -f1 \
    | xargs pip2 --no-python-version-warning --disable-pip-version-check install --upgrade \
    && pip2 --no-python-version-warning --disable-pip-version-check freeze \
    | grep '==' \
    | cut -d'=' -f1 \
    | grep -v 'moin' \
    | xargs pip2 --no-python-version-warning --disable-pip-version-check install --upgrade --force-reinstall \
    && pip2 --disable-pip-version-check check \
    && rm -rf /usr/local/share/man/* \
    && apk del -r --purge --progress build-base libffi-dev \
    && pip2 cache purge \
    && GZIP=-9 pip2 freeze | grep -v -E "moin|xapian" | gzip > /dist/requirements.txt.gz \
    && echo -e "\e[94;1m# Setting a random password for 'root'...\e[0m" \
    && _randpass=$(strings /dev/urandom |tr -dc A-Za-z0-9 | head -c40;echo) \
    && echo -e "$_randpass\n$_randpass" | passwd root \
    && unset _randpass \
    && echo -e "\e[94;1m# Creating '/moin' folder...\e[0m" \
    && mkdir /moin \
    && chmod -R 0777 /moin \
    && chmod 0666 /dist/moin-wiki-files.tar.gz /dist/requirements.txt.gz \
    && python2 -m compileall

RUN touch /entrypoint.sh \
    && chmod 0755 /entrypoint.sh \
    && echo -e "\e[94;1m# Creating the base64 data to be restored upon first boot...\e[0m" \
    && echo -e "read -r -d '' BASE64TARGZ <<'BASE64WIKIFILESBASE64'" >> /tmp/dat.tmp \
    && base64 /dist/moin-wiki-files.tar.gz >> /tmp/dat.tmp \
    && echo -e "BASE64WIKIFILESBASE64\n" >> /tmp/dat.tmp \
    && du -sh /tmp/dat.tmp \
    && echo -e "\e[94;1m# Adding the base64 data to /entrypoint.sh ..Please wait...\e[0m" \
    && sed -i '/##PACKEDDATABEG##/r /tmp/dat.tmp' /entrypoint.sh \
    && rm /tmp/dat.tmp \
    && rm /dist/moin-wiki-files.tar.gz

ENV PYTHONPATH=/usr/local/lib/python2.7/site-packages:/moin/guni-wsgi:/moin/config

# PATH=/opt/bin:${PATH}

EXPOSE 8080

ENTRYPOINT [ "/entrypoint.sh" ]
